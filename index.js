console.log("hello")

//Assignment Operator
let assignmentNumber = 8;

//Arithmetic Operators
// + - * / % 

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//shorthand
assignmentNumber +=2;
console.log(assignmentNumber); //12

//Subtraction/Multiplication/division assignment operator (-=, *=. /=)
assignmentNumber -= 2;
console.log(assignmentNumber);
assignmentNumber *= 2;
console.log(assignmentNumber);
assignmentNumber /= 2;
console.log(assignmentNumber);

//Multiple Operators and Parenthesis
/*
-when multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, exponents, multiplication, division, addition and substraction) rule 
-The operations were done in the ff order:
	1. 3 * 4 =12
	2. 12 / 5 =2.4
	3. 1 + 2 = 3
	4. 3 -2 = 1

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


//Increment and Decrement Operator
//Operators that add or subtract values by 1 and reaassigns the value of the variable where the increment/decrement was applied to

let z = 1;

//increment
//pre-fix incrementation
//the value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment".
++z;
console.log(z); //2 - the value of z was added with 1 and is immediately return.

//post-fix incrementation
//the value of "z" is returened and stored in the variable "increment" then the value of 'z' is increased by one
z++;
console.log(z); //3 - The value of z was added with 1
console.log(z++); //3 - the previous value of the variable is returned.
console.log(z); //4 - a new value is now returned.

//pre-fix vs post-fix incrementation
console.log(z++); //4
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately.


//pre-fix and post-fix decrementation
console.log(--z); //5 with pre-fix decrementation the result of subtraction by 1 is returned immediately

console.log(z--); //5 with post-fix decrementation the result of subtraction by 1 is not immediately returned, instead the previous value is returned first
console.log(z); //4


//type coercion
//is the automatic or implicit conversion of values from one data type to another
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion);


//adding/contatenating a string and a number will result to a string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion)
console.log(typeof nonCoercion);


//the boolean "true" is also associated with the value of 1
let numE = true + 1;
console.log(numE); //
console.log(typeof numE); // number

////the boolean "false" is also associated with the value of 0
let numF = false + 1;
console.log(numF);

//Comparison Operators
let juan = 'juan';

// (==) Equality operator
//checks whether the operands are equal/have the same content
//attempts to convert and compare operands of different data types
//returns a boolean value

console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'JUAN'); //false, case sensitive
console.log('juan' == juan);

//!= inequality operator
//checks whether the operands are not equal/have different content

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false);
console.log('juan'!= 'JUAN');
console.log('juan'!= juan);

//Strictly Equality Operator

console.log("strictly equality operator")
console.log(1 === 1); // true
console.log(1 === 2); // false
console.log(1 === '1'); //false not the same data type
console.log(0 === false); // false different data type
console.log('juan' === 'JUAN'); // false case sensetive
console.log('juan' === juan); // true

//(!==)Strict inequality operator


console.log("strictly inequality operator")
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan'!== 'JUAN'); //
console.log('juan'!== juan);//

//Relational comparison operators
//check the relationship between the operands

let x = 500;
let y = 700;
let w = 8000;
let numString = "5500"

console.log("greater than")
//greater than (>)
console.log(x > y); //false
console.log(w > y); //true 

console.log("lessthan")
//lessthan (<)
console.log(w < x); //false
console.log(y < y); //false
console.log(x < 1000) //true
console.log(numString < 1000) //false - forced coercion
console.log(numString < 6000) //true - forced coercion to change string to number
console.log(numString < "Jose")

//greater than or equal to

console.log(w >= 8000); // true

//less than or equal to
console.log(x <= y); // true
console.log(y <= y); // true


//logical operators

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("logical AND operators")

//logical AND Operator ( && - Double ampersand)
//returns true if all operands are true

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); //false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); //true


let requiredLevel = 95;
let requiredAge = 18;


let authorization3 = isRegistered && requiredLevel === 25;
console.log(authorization3); //false 


let authorization4 = isRegistered &&  isLegalAge && requiredLevel === 95;
console.log(authorization4); // true



let userName = "gamer2022"
let userName2 = "shadow1991"
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= requiredAge;
//.length is a property of strings which determine the number of characters in the string.

console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true


console.log("logical OR Operator");

// OR Operator (|| - double pipe)
//returns true if atleast ONE of the operands are true


let userLevel = 100;
let userLevel2 = 65;


// let guildRequirement1 = is isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
// console.log(guildRequirement1); // true


let guildRequirement2 = isAdmin || userLevel2 >= requiredLevel;
console.log(guildRequirement2);//false

console.log("Not operator")
//NOT operator (!)
//turns a boolean into the opposite value 


let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);//true


console.log(!isRegistered); //false
console.log(!isLegalAge); //false

let opposite1 = !isAdmin;
let opposite2 = !isLegalAge;

console.log(opposite1); //true isAdmin orginal value is = false
console.log(opposite2); //false isLegalAge original value = true


//if , else if , and else statement

//IF statement
//if statement will run a code block if the condition specified is true or results to true

// if(true){
// 	alert("We just run an if condition")
// }


let numG = -1;

if(numG < 0){
	console.log('Hello');
}

let userName3 = "crusader1993";
let userLevel3 = 25;
let userAge3 = 20;

if(userName3.length > 10){
	console.log("Welcome to game Online");
}
if(userLevel3 >= requiredLevel) {
	console.log("You are qualified to join the guild!")
}

if(userName3.length >= 10 && isRegistered && isAdmin){
	console.log("Thank you for joining the Admin!")
} else{
	console.log("You are not ready to be an Admin")
}

//ELSE statement
//The "else" statement executes a block of codes if all other conditions are false

if(userName3.length >= 10 && userLevel3 >= requiredLevel && userAge3 >= requiredAge){
	console.log("Thank you for joining Noobies Guild!")
} else {
	console.log("You are too strong to be a noob. :( ")
}


//else if statement
//else if executes a statement if the previous or the original condition is false or resulted to false but another specified condition resulted to true.
if(userName3.length >= 10 && userLevel3 <= 25 && userAge3 >= requiredAge){
	console.log("Thank you for joining the noobies guild.")
} else if(userLevel >25){
	console.log("You are too strong to be a noob.")
} else if (userAge3 < requiredAge){
	console.log("You're too young to join the guild.")
} else{
	console.log("better luck next time.")
}


//if, else if and else statement with functions

function addNum(num1, num2) {
	//check if the numbers is being passed are number types.
	//typeof keyword returns a string or which tells the type of data that follows it
	if(typeof num1 === "number" && typeof numb2 === "number") {
		console.log("Run only if both arguments passed are number types");
		console.log(num1 + num2);
	} else{
		console.log("One or both of the arguments are not numbers")
	}
}

addNum(5, '2');

let numsample = true;
console.log(typeof numsample)


//create log in function

function login(username, password){
	//check if the argument passed are strings
	if(typeof username === "string" && typeof password === "string"){
		console.log("both arguments are strings.")
		/*
			nested if-else
				will run if the parent if statement is able to agree to accomplish is condition
		*/
	} if(username.length >= 8 && password.length >= 8){
		console.log("Thank you for logging in")
	}
	else{
		console.log("credential is too short")
	}
}

login("janejane", "12345678")


//function with return keyword

let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet. '
	}
	else if(windSpeed <= 61){
		return 'Trophical depression detected.'
	}
	else if(windSpeed >= 62 && windSpeed <= 88){
		return 'Trophical storm detected'
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected.'
	}
	else{
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);

//console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.

if (message == 'Trophical storm detected'){
	console.warn(message);
}

//truthy and falsy

if(true){
	console.log('Truthy')
}
if (1){
	console.log('True')
}

if([]){
	console.log('Truthy')
}

//flasy
//-0, "", null, NaN
if(false){
	console.log('Falsy')
}

if(0){
	console.log('False')
}

if(undefined){
	console.log('Falsy')
}

//Ternary Operator
/*
Syntax:
	(expression/condition) ? ifTrue : ifFalse;
three operands of ternary operator:
1.condition
2. expression to execute if the condition is truthy
3. expression to execute if the condition is false

*/


// let ternaryResult = (1 < 18) ? true : false;
// console(`Result of ternary operator ${ternaryResult}`);

// if(1<18){
// 	console.log(true)
// }else{
// 	console.log(false)
// }

let price = 50000;

price > 1000 ? console.log("price is over 1000") : console.log("price is less than 1000")



let villain = "Harvey Dent";

villain === "Two Face"
? console.log("You lived long enough to be a villain")
: console.log("Not quite villainous yet.")

//Ternary operators have an implicit "return" statement that without return keyword, the resulting expression can be stored in a variable.


//else if ternary operator

let a = 7;
a === 5
? console.log("A")
: (a === 10 ? console.log("A is 10") : console.log("A is not 5 or 10"))


let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit'
}


function isUnderAge(){
	name = "Jane";
	return 'You are under the age limit'
}

// let age = parseInt(prompt("what is your age?"));
// console.log(age);

// //? specifies the 'if'
// //() specifies the condition
// // : specifies the "else"

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log(`Result of Ternary operator in function: ${legalAge}, ${name}`);



//parseInt() - convert a string to a number

//Switch statement
/*
Syntax:
	switch (expression/condition){
	case value:
		statement;
		break;
	default
		statement;
		break;
	}

*/

///////////////sample
// let hero = prompt("Type a hero").toLowerCase();

// switch (hero) {
// 	case "jose rizal":
// 		console.log("National Hero of the Philippines");
// 		break;
// 	case "george washington":
// 		console.log("Hero of the America Revolution");
// 		break;
// 	case "hercules":
// 		console.log("Legendary Hero of the Greek");
// 		break;
// 	default:
// 		console.log("Please Type Again");
// 		break;
// }

function roleChecker(role) {
	switch(role) {
		case "admin":
			console.log("Welcome Admin, to the dashboard.")
			break;
		case "user":
			console.log("You are not authorized to view this page.")
			break;
		case "guest":
			console.log("Go to the registration page to register.")
			break;
		default:
			console.log("Invalid Role")
			//by default, your switch ends with default case, so therefore, even if there is no break keyword in your default case, it will not run anything else.
	}
}

roleChecker("admin");


//Try-Catch-Finally Statement
//this is used for error handling


function showIntensityAlert(windSpeed){
	//Attempt to execute a code
	try {
		alerat(determineTyphoonIntensity(windSpeed))
	}catch (error) {
		//error/err are commonly used variable by developers for storing errors
		console.log(typeof error);

		//Catch errors within 'try'
		//the "error.message" is used to access the information relating to an error object
		console.warn(error.message);
	}
	finally {
		//COntinue execution of code REGARDLESS of success or failure of code execution in the 'try' block to handle/resolve errors.
		//optional
		alert('Intensity updates will show alert')
	}
}

showIntensityAlert(68);

//throw - user -defined exception
//Execution of the current function will stop


const number = 40;

try {
	if (number > 50) {
		console.log('Success');
	} else {
		//user-defined throw statement
		throw Error('The number is low');
	}
	//if throw executes, the below code does not execute
	console.log('hello')
}
catch (error) {
	console.log('An error caught');
	console.warn(error.message)
}

finally{
	console.log("Please add a higher number")
}


//another example

function getArea(width, height) {
	if(isNaN(width) || isNaN(height)){
		throw 'Parameter is not a number!'
	}

}

try {
	getArea(3, 'A');
}
catch(e) {
	console.error(e)
}

finally {
	alert("Number only")
}














